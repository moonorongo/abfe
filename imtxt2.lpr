program imtxt2;

uses SysUtils, CastleWindow, CastleGLImages, CastleFilesUtils, CastleKeysMouse,
  CastleRectangles, CastleGameNotifications, ShellApi, JwaWindows,
  CastleColors, CastleControls, fpjson, jsonparser;

type
  // registro de configuracion de la app
  TABConfig = record
    keyUp : integer;
    keyDown : integer;
    keyLeft : integer;
    keyRight : integer;
    keySelect : integer;
    screenWidth : integer;
    screenHeight : integer;
    enableBackgroud : boolean;

    MamePath : String;
    SnapPath : String;
  end;

  // registro que guarda los datos de la grilla
  TSnaps = record
    Image : TGLImage;
    romFile : string;
    title : string;
    sourceRect : TRectangle;

    scaledWidth : integer;
    scaledHeight : integer;
    marginLeft : integer;

  end;

  // registro que guarda los datos del cursor
  TCursor = record
    x : integer;
    y : integer;

    screen_x : integer;
    screen_y : integer;
    screenTop : integer;

    dx : shortint;
    dy : shortint;
    Image : TGLImage;
  end;


const
   velocity_y = 80;
   velocity_x = 120;
   Image_Transparent : TCastleColor = (Data: ( 1.0 , 1.0 , 1.0 , 0.2));

var
  Window: TCastleWindow;

  Images : array[0..64] of TSnaps;
  totalImages : integer;

  Cursor : TCursor;

  BGImage : TGLImage;
  bgMovementRatio : Single;
  diffScreenBg : integer;

  FE_JSON_Config : TJSONData;
  AB_Config : TABConfig;

  columnsPerRow : integer; // columnas por fila (columnas de la pantalla)
  rowsPerPage : integer;   // filas por pantalla
  totalRows : integer;     // total de filas
  boxWidth : integer;      // ancho de la grilla
  boxHeight : integer;     // alto de la grilla
  screen_margin_left : integer;     // margen izq de pantalla

  originDisplace : integer; // desplazamiento del eje de coodenadas (hacia arriba)
  initialRow : integer;     // fila inicial de scroll (0)
  initialY : integer;       // posicion inicial Y scroll
  scrollFlag : Boolean;     // si scrollea o mueve cursor
  isRunning : Boolean;      // evita que se disparen muchos mame cuando pulso select
  selected_index : integer; // indice de la rom que se esta corriendo
  selected_counter : integer;  // contador que se establece cuando selecciono una rom


procedure RunShellExecute(const prog,params:string);
begin
  //  ( Handle, nil/'open'/'edit'/'find'/'explore'/'print',   // 'open' isn't always needed
  //      path+prog, params, working folder,
  //        0=hide / 1=SW_SHOWNORMAL / 3=max / 7=min)   // for SW_ constants : uses ... Windows ...

  if ShellExecute(SW_SHOWNORMAL,'open',PChar(prog),PChar(params),PChar(extractfilepath(prog)),1) >32 then; //success
end;


procedure WindowRender(Container: TUIContainer);
var
   index : integer;
   i : integer;
   j : integer;
   X: Single = 0.0;
   Y: Single = 0.0;
   textRect : TRectangle;
   screenRect: TRectangle;

   titleX : integer;
   titleY : integer;
   title : string;

begin
  if AB_Config.enableBackgroud then
    BGImage.Draw(0, (initialY  * bgMovementRatio) - diffScreenBg);

  for i := 0 to totalRows do
    for j := 0 to columnsPerRow - 1 do
    begin
      X := j * boxWidth;
      Y := initialY + originDisplace - (i * boxHeight);

      index := i * columnsPerRow + j;
      if(index > totalImages - 1) then Break;

      title := Images[index].title;

      screenRect.Left := trunc(X) + 10 + screen_margin_left;
      screenRect.Bottom := trunc(Y) + 8;
      screenRect.Width := 300;
      screenRect.Height := 240;


      if ( (screenRect.Bottom < AB_Config.screenHeight) and
           (screenRect.Bottom + boxHeight >= 0) ) then
      begin

        // si selecciono algo lo resalta
        // al cabo de cierto tiempo pone todo igual
        if (selected_counter > 0)  then
        begin
          selected_counter := selected_counter - 1;
          if (index = selected_index) then
          begin
            Images[index].Image.Color := White;
          end
          else
          begin
            Images[index].Image.Color := Image_Transparent;
          end;
        end
        else
        begin
          Images[index].Image.Color := White;
        end;

        Images[index].Image.Draw(screenRect, Images[index].sourceRect);

        titleX := trunc(X);
        titleY := trunc(Y);

        textRect.Bottom := titleY;
        textRect.Left := screen_margin_left + titleX;
        textRect.Height := 40;
        textRect.Width := 320;
        UIFont.PrintRect(textRect, Black, title, hpMiddle, vpTop);

        textRect.Bottom := titleY - 1;
        textRect.Left := screen_margin_left + titleX + 2;
        textRect.Height := 40;
        textRect.Width := 320;
        UIFont.PrintRect(textRect, White, title, hpMiddle, vpTop);
      end;

    end;

  Cursor.Image.Draw( screen_margin_left + Cursor.screen_x * 1.0,
                    (originDisplace - Cursor.screen_y) * 1.0,
                    320,
                    256);

//UIFont.Print(0, 0, White, FloatToStr(bgMovementRatio));
//UIFont.Print(320, 0, White, intToStr(initialY));
end;


procedure CalcBoxSize;
begin
  columnsPerRow := trunc(AB_Config.screenWidth / 320);
  rowsPerPage :=  trunc(AB_Config.screenHeight / 256);

  boxWidth := trunc(AB_Config.screenWidth / columnsPerRow);
  boxHeight := trunc(AB_Config.screenHeight / rowsPerPage);

  screen_margin_left := trunc((boxWidth - 320) / 2);
  originDisplace := AB_Config.screenHeight - boxHeight;

  // ver por que screen_margin_left no alinea bien... ver... ;
end;


procedure LoadConfig;
var
  json : String;
  line : String;
  tfIn: TextFile;
begin
  json := '';

  AssignFile(tfIn, 'config.json');
  reset(tfIn);

  while not eof(tfIn) do
  begin
    readln(tfIn, line);
    json := json + line;
  end;

  CloseFile(tfIn);

  FE_JSON_Config := GetJSON(json);

  AB_Config.MamePath := FE_JSON_Config.FindPath('mamePath').AsString;
  AB_Config.SnapPath := FE_JSON_Config.FindPath('snapPath').AsString;

  AB_Config.keyUp := FE_JSON_Config.FindPath('keyUp').AsInteger;
  AB_Config.keyDown := FE_JSON_Config.FindPath('keyDown').AsInteger;
  AB_Config.keyLeft := FE_JSON_Config.FindPath('keyLeft').AsInteger;
  AB_Config.keyRight := FE_JSON_Config.FindPath('keyRight').AsInteger;
  AB_Config.keySelect := FE_JSON_Config.FindPath('keySelect').AsInteger;
  AB_Config.screenHeight := FE_JSON_Config.FindPath('screenHeight').AsInteger;
  AB_Config.screenWidth := FE_JSON_Config.FindPath('screenWidth').AsInteger;
  AB_Config.enableBackgroud := FE_JSON_Config.FindPath('enableBackground').AsBoolean;

  Cursor.x := 0;
  Cursor.y := 0;
  Cursor.dx := 0;
  Cursor.dy := 0;

  initialY := 0;
  initialRow := 0;
  CalcBoxSize;
end;


procedure LoadImages;
var
   json_games : TJSONData;
   i: integer;
   romFile : string;

   imageRect : TRectangle;

   BG_rect : TRectangle;
   totalRowsFloat : double;
   aspectRatio : Single;

begin
  json_games := FE_JSON_Config.FindPath('games');
  totalImages := json_games.Count;

  for i := 0 to totalImages - 1 do
  begin
    romFile := json_games.Items[i].FindPath('name').AsString;
    Images[i].Image := TGLImage.Create(AB_Config.SnapPath + romFile + '.png');

    imageRect := Images[i].Image.Rect;
    aspectRatio := imageRect.Width / imageRect.Height;

    if aspectRatio >= 1.25 then
    begin // mas ancho que alto
      Images[i].sourceRect.Width := trunc(imageRect.Height * 1.25);
      Images[i].sourceRect.Height := imageRect.Height;
      Images[i].sourceRect.Left := trunc( (imageRect.Width - Images[i].sourceRect.Width) / 2 ); // margin left
      Images[i].sourceRect.Bottom := 0;
    end
    else
    begin // mas alto que ancho
      Images[i].sourceRect.Height := trunc(imageRect.Width / 1.25);
      Images[i].sourceRect.Width := imageRect.Width;
      Images[i].sourceRect.Left := 0;
      Images[i].sourceRect.Bottom := trunc( (imageRect.Height - Images[i].sourceRect.Height) / 2 ); // margin bottom...
    end;

    Images[i].romFile := romFile;
    Images[i].title := json_games.Items[i].FindPath('title').AsString;
  end;

  Cursor.Image := TGLImage.Create(AB_Config.SnapPath + 'cursor.png');

  totalRowsFloat := totalImages / columnsPerRow;
  totalRows := trunc(totalRowsFloat);

  // caso particular en que quedan espacios vacios
  // o sea, la division no da perfecta, incremento .
  if totalRowsFloat <> totalRows then
    totalRows := totalRows + 1;

  if(AB_Config.enableBackgroud) then
  begin
    BGImage := TGLImage.Create(AB_Config.SnapPath + 'bg2.jpg');
    BG_rect := BGImage.Rect;
    diffScreenBg := BG_rect.Height - AB_Config.screenHeight;

    // si hay mas filas que filas por pantalla
    if totalRows > rowsPerPage then
      bgMovementRatio := (diffScreenBg / (totalRows - rowsPerPage)) / boxHeight
    else
      bgMovementRatio := 0;
  end;
end;


procedure FreeImages;
var
   i : integer;
begin
  for i := 0 to Length(Images) - 1 do
    FreeAndNil(Images[i].Image);

  FreeAndNil(Cursor.Image);
  FreeAndNil(BGImage);
end;




procedure WindowUpdate(Container: TUIContainer);
var
  dest_x : integer;
  speed_x : integer;
  dest_y : integer;
  speed_y : integer;
  index : integer;

begin
  if isRunning then
  begin
    isRunning := false;
    Sleep(250);
    RunShellExecute(AB_Config.MamePath, Images[selected_index].romFile);
  end;


  // si presiona select, resalta el seleccionado y pone el flag para ejecutar el mame
  if ( Container.Pressed[TKey(Ord(AB_Config.keySelect))] ) and
     ( not isRunning ) and
     (selected_counter = 0) then
  begin
    index := ((initialRow + Cursor.y) * columnsPerRow) + Cursor.x;

    if index < totalImages then
    begin
      selected_index := index;
      selected_counter := 500;
      isRunning := true;
    end;
  end;

{
  if Container.Pressed[TKey(27)] then
  begin
    halt;
  end;
}

// X movement
  if Cursor.dx = 0 then // static
  begin
    if Container.Pressed[TKey(Ord(AB_Config.keyLeft))] then
    begin
      if(Cursor.x > 0) then
      begin
        Cursor.dx := -1;
        Cursor.x := Cursor.x - 1;
      end;
    end;

    if Container.Pressed[TKey(Ord(AB_Config.keyRight))] then
    begin
      if(Cursor.x < columnsPerRow - 1) then
      begin
        Cursor.dx := 1;
        Cursor.x := Cursor.x + 1;
      end;
    end;
  end
else
  begin // in movement
    dest_x := Cursor.x * boxWidth;
    speed_x := trunc((abs(Cursor.screen_x - dest_x) / boxWidth) * velocity_x) + 2;

    if Cursor.dx > 0 then
    begin
      if Cursor.screen_x >= dest_x  then
      begin
        Cursor.dx := 0;
        Cursor.screen_x := dest_x;
      end;
    end
  else
    begin
      if Cursor.screen_x <= dest_x  then
      begin
        Cursor.dx := 0;
        Cursor.screen_x := dest_x;
      end;
    end;

    // update cursor position
    Cursor.screen_x := Cursor.screen_x + (Cursor.dx * speed_x);
  end; // end in movement



// -------------------------------
// Y movement
  if Cursor.dy = 0 then // static
  begin
    if Container.Pressed[TKey(Ord(AB_Config.keyUp))] then
    begin
      if(Cursor.y > 0) then // no esta en top margin
      begin
        Cursor.dy := -1;
        Cursor.y := Cursor.y - 1;
        scrollFlag := false;
      end
      else // esta en top margin
      begin
        if(initialRow > 0) then
        begin
          Cursor.dy := -1;
          initialRow := initialRow - 1;
          scrollFlag := True;
        end;
      end;
    end;

    if Container.Pressed[TKey(Ord(AB_Config.keyDown))] then
    begin
      if(Cursor.y < rowsPerPage - 1) then
      begin
        Cursor.dy := 1;
        Cursor.y := Cursor.y + 1;
        scrollFlag := False;
      end
      else
      begin
        if initialRow < totalRows - rowsPerPage then
        begin
          Cursor.dy := 1;
          initialRow := initialRow + 1;
          scrollFlag := True;
        end;
      end;
    end;

  end
  else
  begin // in movement
    if scrollFlag then
    begin // Background scroll
      dest_y := initialRow * boxHeight;
      speed_y := trunc((abs(initialY - dest_y) / boxHeight) * velocity_y) + 2;

      if Cursor.dy > 0 then
      begin
        if initialY >= dest_y  then
        begin
          Cursor.dy := 0;
          initialY := dest_y;
        end;
      end
      else
      begin
        if initialY <= dest_y  then
        begin
          Cursor.dy := 0;
          initialY := dest_y;
        end;

      end;

      // update cursor position
      initialY := initialY + (Cursor.dy * speed_y);

    end
    else
    begin // Cursor movement
      dest_y := Cursor.y * boxHeight;
      speed_y := trunc((abs(Cursor.screen_y - dest_y) / boxHeight) * velocity_y) + 2;

      if Cursor.dy > 0 then
      begin
        if Cursor.screen_y >= dest_y  then
        begin
          Cursor.dy := 0;
          Cursor.screen_y := dest_y;
        end;
      end
      else
      begin
        if Cursor.screen_y <= dest_y  then
        begin
          Cursor.dy := 0;
          Cursor.screen_y := dest_y;
        end;

      end;

      // update cursor position
      Cursor.screen_y := Cursor.screen_y + (Cursor.dy * speed_y);
    end;
  end; // end in movement



end; // end function



begin
  selected_index := 0;
  selected_counter := 0;
  ShowCursor(false);
  isRunning := false;
  LoadConfig;
  LoadImages;

//  readln();

  try
    Window := TCastleWindow.Create(Application);
    Window.FullScreen := False;
    Window.Left:=0;
    Window.Top := 0;
    Window.Height:=768;
    Window.Width:=1024;
    Window.ResizeAllowed := raNotAllowed;
    Window.MainMenuVisible:= False;
    Window.SwapFullScreen_Key := K_None;

    Window.OnRender := @WindowRender;
    Window.OnUpdate := @WindowUpdate;

    Window.Open;

    Application.Run;

  finally
    ShowCursor(true);
    FreeImages;
    Window.Free;
  end;

end.



